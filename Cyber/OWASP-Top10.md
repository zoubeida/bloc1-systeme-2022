## OWASP 
---
avril 2023 
 

#### Références 

- Site de référence : <https://owasp.org/>
	- TOP 10 : https://owasp.org/www-project-top-ten/
	- Sur Github en Fr : https://github.com/OWASP/Top10/blob/master/2021/docs/index.fr.md

- Vidéos : 
	- TOP 10 (2021) : <https://youtu.be/xe9LN2w7hfE>  
 	- TOP 10 (2017-2021) : <https://youtu.be/Q5KB2KrNzlA> et sa Playlist **explications des failles** : <https://youtube.com/playlist?list=PLr3lUE_48stofSVmN1y0KPiSGYDNxHRvE>


#### Travail à Réaliser  

Pour Chacune des 10 failles les plus importantes, proposer :
- sa définition, 
- les recommendations de l'OWASP pour l'éviter
- si en tant que développeurs,nous sommes concernés

à la fin, noter les mots clés incompréhensibles qu'il faudrait définir

Livrer un compte-Rendu ***personnel*** : `B1_Systeme/CR_OWASP-Top10.md` 
