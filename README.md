# Bloc1-Systeme

***Ressources*** pour le module Bloc1 (Système)

#### sites de référence utiles
- sommaire des commandes : <https://ss64.com/nt/>
- les langages personnifiés : <https://www.youtube.com/watch?v=lDjZA0wodGQ>
- cmd windows en 10 minutes :<https://www.youtube.com/watch?v=Jfvg3CS1X3A>
- bash linux en 10 minutes :<https://www.youtube.com/watch?v=gd7BXuUQ91w>

##### 14/09/2022

- Prise de contact et création des dépôts gitlab.
- **Mission0** : Readme.md avec une descriptions de soi et l'intégration d'une photo 

##### 28/09/2022

- Début du cours sur les composants : [accessible sur gitlab](B1-Systeme_cours1-Composants.pdf)
- Lancement Mission1 : [accessible sur gitlab](B1-Systeme_Mission1.md)
- Accès en ligne de commande à Gitlab avec l'outil Gitbash sur Windows/MacOS.(Quelques commandes utiles vues en classes (bash linux) ou commandes git spécifiques)

##### 12/10/2022

- **Mission2** : [accessible sur gitlab](B1-Systeme_Mission2.md)
Travail d'équipe sur <https://gitlab.com/ESIEE-2022-2024/COMPOSANTS_PC>
	
	Ce travail, s'il est réussi, permettra à tous les présents de valoriser leur travail. **Cette production permettra de réviser pour le DS de la séance du 09/11/2022.** 

##### 26/10/2022

- **Mission2** : [accessible sur gitlab](B1-Systeme_Mission2.md)
  Travail à compléter avec pandoc.
	
- Fin du cours sur les composants : [accessible sur gitlab](B1-Systeme_cours1-Composants.pdf)

##### 09/11/2022

- **Devoir Surveillé** sur les composants d'ordinateurs, format markdown, et sur git (aucun document permis)

##### 23/11/2022

- Cours sur Système et OS [ici](B1-Systeme_cours2-SystemeExploitation.md)
- Mission3 sur les commandes linux et windows : [ici](B1-Systeme_Mission3.md)

##### 07/12/2022

- Dernier cours dédié à la Mission3 sur les commandes linux et windows : [ici](B1-Systeme_Mission3.md)

##### 04/01/2023

- Fin du cours sur Système et OS [ici](B1-Systeme_cours2-SystemeExploitation.md)
- retour sur la Mission3 avant le Devoir Surveillé : [ici](B1-Systeme_Mission3.md)

##### 19/01/2023

- **Devoir Surveillé** sur le système/OS, commandes windows ou linux et sur git (anti-sèche permise si tableau markdown transormé en pdf avec pandoc - 2 pages max).
- Première mission du S2 : [Mission AlwaysData](B1_MissionAlwaysData.md)
