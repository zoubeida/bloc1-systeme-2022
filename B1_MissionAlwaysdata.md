## Mission : Déploiement sur un serveur distant avec AlwaysData 
 [Zoubeïda ABDELMOULA](mailto:zoubeida.abdelmoula@gmail.com), ESIEE-IT
 Bloc1 Système (BTS - SIO) 04/01/2022






## Objectif

Pour déployer une application web sur Internet (Ex: Portfolio ou autre), on a besoin d'un serveur web hébergé dans le nuage (Cloud). 

L'objectif de cette activité est d'apprendre à utiliser un serveur hébergé et déjà installé et de le configurer selon vos besoins. Pour garder une trace de vos découvertes, il faut aussi  créer une documentation qui recence toutes les étapes effectuées (en markdown et pdf).

A déposer sur GitLab sous le nom : `B1_Systeme/Mission_Alwaysdata.md` et `B1_Systeme/Mission_Alwaysdata.pdf`

Voici quelques sites de référence :

- <https://www.alwaysdata.com/fr/>

- <https://technique.arscenic.org/transfert-de-donnees-entre/article/scp-transfert-de-fichier-a-travers>

- <https://www.digitalocean.com/community/tutorials/how-to-use-rsync-to-sync-local-and-remote-directories>



## Etape 0 : Créer un espace sur Alwaysdata

Créer un compte sur Alwaysdata gratuit (100 Mo).

- Quels services sont offerts par Alwaysdata ?  
- Quels services seront nécessaires pour déployer un site web (HTML, CSS, JS et PHP) ?
- Quel est le nom de domaine choisi pour le déploiement de votre site ?
 
## Etape 1 : Activer SSH

Pour ajouter les fichiers de votre site web à votre espace, il faut activer votre accès ssh.

- Expliquer l'intérêt du protocole SSH. Sur quel port est-il actif par défaut ? 
- Quel autre protocole semble avoir les mêmes fonctionnalités ? Que fait SSH qui n'est pas possible avec le 2e ? 
- Activer un accès au serveur via ce protocole. Quelles étapes sont nécessaires ?  
- Se connecter à votre espace dédié sur le serveur via ce protocole. Quelle est la ligne de commande nécessaire pour y arriver ? 
- Dans quel repertoire faut-il déposer vos fichiers du site si vous voulez le voir en ligne ? (chemin complet sur le serveur) 

## Etape 2 : Copier notre contenu sur Alwaysdata

Ajouter les fichiers du site sauvegardé en local au répertoire distant dédié.

- Quel est le chemin local absolu pour accéder à votre site ?
- Quel est le chemin absolu du repertoire dédié sur le serveur Alwaysdata ?
- Les commandes `scp` et `rsync` peuvent être d'une grande aide à cette étape. Pourquoi ? 
- Quelle est la différence entre les deux commandes ?
- Quelle est la commande complète pour ajouter les fichiers sauvegardés en local sur le serveur dédié ?
- Comment vérifier que l'ajout a bien été effectué ? Détailler la procédure et les résultats attendus.
- Quelle URL permet de voir votre site en ligne ? 

## Etape 3 - Gestion de paires de clés privée et publique

On souhaiterait se connecter avec une paire de clés privée/public au service SSH accessible sur AlwaysData.

Voici des liens pour vous y aider : 
- <https://www.remipoignon.fr/authentification-ssh-par-cle-privee/>
- Tutoriel d'Alexis Meyer : [ici](TutoCleSSH_AMeyer.md)

- Noter les étapes nécessaires pour y parvenir.
- Notez les étapes pour en tester le bon fonctionnement 