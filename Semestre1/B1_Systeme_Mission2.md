## Mission 2 : Découverte des composants d'une Solution technique d'accès (STA)
#### [Zoubeïda ABDELMOULA](mailto:zoubeida.abdelmoula@gmail.com), ESIEE-IT
#### Cours Bloc1 Système (BTS - SIO) - 12/10/2022





## Objectifs


Les objectifs de cette activité sont : 
- de découvrir les composants d'un PC portable ou fixe et de décrire leur utilité pour le bon fonctionnement de la STA correspondante.
- de travailler en équipe sur un même projet git (avec gestion des tâches)
- expérimenter les outils d'édition et de publication (markdown et pandoc)

L'utilisation d'outils d'édition collaboratifs open-source est proposé aussi 
- FramaPad :<https://mensuel.framapad.org/>
- Git et Gitlab <https://gitlab.com/ESIEE-2022-2024/COMPOSANTS_PC/>

Lors de cette activité, l'outil Pandoc sera utilisé pour générer des documents PDF à partir du document markdown.

- <https://pandoc.org/getting-started.html>


## Description de plusieurs composants physiques élémentaires

Un document conjoint , réalisé par toute la classe, est attendu. On doit y trouver une description de la majorité des compsants d'une STA portable ou Fixe. 

La description doit être élaborée pour permettre **à des collégiens** de mieux connaître le composant : 
- son utilité, son fonctionnement.
- ses caractéristiques, ses modes de branchement.  
- son impact environnemental (réustilisation, recyclage)
- Marques connues (Open Hardware ?)

Un document global identique et mis en forme sera déposé sur un dépôt commun. Chacun utilisera Pandoc pour générer le format PDF.

Continuer à compléter votre aide mémoire personnel avec tous les nouveaux marqueurs markdown découverts ou utilisés et déjà enregistré sous le nom `B1-Systeme/aide_markdown.md`


## A la fin de l'activité...

On devra trouver sur votre dépôt 2 documents (+ les images) : 

- votre premier aide-mémoire mis à jour : `B1-Systeme/aide_markdown.md`
- la description de tous les composants : `B1-Systeme/Composants.md` et en pdf `B1-Systeme/Composants.pdf`
- vos images personnelles dans le repertoire : `B1-Systeme/img`

On devra voir vos contributions sur le dépôt global en markdown et pdf : 
-  <https://gitlab.com/ESIEE-2022-2024/COMPOSANTS_PC/>

